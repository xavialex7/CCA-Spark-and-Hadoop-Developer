/**
  * Importar todo un paquete
  * import java.io._
  *
  */

class Fraction(numerator: Int, denominator: Int) {
  println("The numerator is " + numerator)
  println("The denominator is " + denominator)
  println("Fraction: " + numerator + "/" + denominator + ". Decimal: " + (numerator / denominator).toDouble)
}

val a = new Fraction(12, 3)

def ADD(a: Int, b: Int): Int = {
  a + b
}

val b = ADD(2, 6)

class Animal(name: String, var legs: Int) {
  /**
    * Por defecto, los parámetros de una clase son val
    * val hace la variable inmutable, esto es, no se puede alterar
    * Con var se consigue que pueda alterarse
    * Así pues, el nombre de la clase no se podría alterar (sólo tiene getter)
    * pero sí podrían alterarse el nº de patas (tiene getter y setter)
    */
  def this(name: String) = this(name, 4)
  def this() = {
    this("Animal", 4)
    println("Constructor with no values")
  }
  println("Name: " + name + ". Legs: " + legs)
}

val c = new Animal()
val d = new Animal("Bull")
val e = new Animal("Dog", 4)



helloworld.main(Array(" "))
