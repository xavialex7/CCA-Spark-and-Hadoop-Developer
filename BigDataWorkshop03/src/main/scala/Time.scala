/**
  * Created by festevem on 30/03/2017.
  */
class Time(Hours: Int, Minutes: Int, Seconds: Int) {

  require((Hours >= 0 && Hours < 60), "Invalid time")

  def minutesOfDay: Int = {
    Minutes
  }

  def apply(Hours: Int, Minutes: Int, Seconds: Int) = {
    println("Método apply")
  }

  println(Hours + ":" + Minutes + ":" + Seconds)
}

// Companion Object
object Time {
  def main(args: Array[String]): Unit = {
  println("Tiempo")
  }

  def apply(Hours: Int, Minutes: Int, Seconds: Int): Unit = {
    val a = new Time(10, 5, 5)
  }
}
