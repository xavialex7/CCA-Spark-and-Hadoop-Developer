// Tratamiento de colecciones
// Listas
val ls = (1 to 10).toList
ls.head
ls.tail
ls.size
ls(2)
ls.take(6)
ls.take(9)
ls.tail
ls.takeRight(9)
ls.contains(7)

ls.::(0)
val ls1 = (10 to 15).toList
ls ++ ls1
ls ++ (1 to 5).toList

// Sets (no hay duplicados)
// Tuples (la lista es homogénea, y la tupla heterogénea)
val t = (1, "Hola")
val l = List((1, "Hola"), (2, "Mundo")) // lista de tuplas

// Maps (Pares clave -> valor)
// Array

val ls2 = (1 to 100).toList
// Suma de elementos de la lista (Mal)
var total = 0
for(value <- ls2) {
  total += value
}


println(total)

// Suma de elementos de la lista (Bien)
ls2.sum

// Anonymous functions
ls2.filter(x => {
  x % 2 == 0
})

ls2.filter(x => x % 2 == 0).sum

ls2.filter(x => {
  x % 2 == 0
}).sum

val t1 = List((1, "Hola"), (2, "Mundo"), (3, "How"), (4, "Are"), (5, "You"), (6, "Doing"))
t1(0)
t1(1)
t1(0)._1 // Primer elemento de la primera tupla
t1.map(x => x._1) // Primeros elementos de todas las tuplas en t1
t1.map(x => x._1).sum // Suma de todos los primeros elementos de las tuplas en t1
t1.map(x => x._1).max
t1.map(x => x._1).min
t1.map(x => x._2)
t1.sortBy(k => k._2)

